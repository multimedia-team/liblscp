Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: liblscp
Upstream-Contact: Rui Nuno Capela <rncbc@users.sourceforge.net>
Source: https://www.linuxsampler.org/downloads.html#liblscp

Files: *
Copyright:
 2004-2024 Rui Nuno Capela <rncbc@users.sourceforge.net>
License: LGPL-2.1+

Files: debian/*
Copyright:
 2010-2012 Alessio Treglia <alessio@debian.org>
 2007 Free Ekanayaka <freee@debian.org>
 2005 Paul Brossier <piem@debian.org>
 2004-2005 Matt Flax <flatmax@pgb.unsw.edu.au>
 2004 Christian Schoenebeck <cuse@users.sourceforge.net>
 2016 Jaromír Mikeš <mira.mikes@seznam.cz>
 2021-2024 Dennis Braun <snd@debian.org>
License: LGPL-2.1+

Files: examples/example_client.c
       examples/example_server.c
Copyright: 2004-2021, rncbc aka Rui Nuno Capela
License: GPL-2+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
Comment:
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
 
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".
